import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class ProductCounter {

	final static Map<String, Integer> PRODUCTS_MAP = new HashMap<String, Integer>();
	static {
		PRODUCTS_MAP.put("Miel", 1200);
		PRODUCTS_MAP.put("Chocolat", 522);
		PRODUCTS_MAP.put("Pomme", 347);
		PRODUCTS_MAP.put("Concombre", 701);
	};

	public static void main(String[] args) throws InterruptedException, ExecutionException {

		ExecutorService counterThreadService = Executors.newSingleThreadExecutor();
		try {
			Future<Integer> sommeProduits = counterThreadService.submit(() -> {
				Stream<Integer> streamValues = PRODUCTS_MAP.values().stream();
				Optional<Integer> optResult =  streamValues.reduce(Integer::sum);
				return optResult.isPresent() ? optResult.get() : null;
			});
			
			//exploitation du r�sultat du thread counterThread.
			System.out.println("Nombre total de produits dans le magasin : " + sommeProduits.get(3, TimeUnit.SECONDS));
			
		} catch (Exception e) {
			System.out.println("L'ex�cution du thread ne s'est pas bien pass�e " + e.getMessage());
		} finally {
			if (counterThreadService != null) counterThreadService.shutdown();
		}
				
		// Traitement principal du thread main()
	}

}
