import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

public interface ExecutorService {
	
	void 	execute(Runnable command);
	
	<T> Future<T> 	submit(Callable<T> task);
	
	<T> Future<T> 	submit(Runnable task, T result);
	
	<T> T 	invokeAny(Collection<? extends Callable<T>> tasks);
	
	<T> List<Future<T>>  invokeAll(Collection<? extends Callable<T>> tasks);
	
	void 	shutdown();
}
