import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ProductDisplayer3 {

	final static List<String> PRODUCTS_LIST = Arrays.asList("Miel", "Chocolat", "Pomme", "Chocolat");

	public static void main(String[] args) {

		ScheduledExecutorService displayerThreadService = Executors.newSingleThreadScheduledExecutor();
		try { 
			LocalDateTime dateDebut = LocalDateTime.now();
			System.out.println("Date de d�but : " + dateDebut);
			
			Runnable command = () -> {
				LocalDateTime dateDemarrage = LocalDateTime.now();
				System.out.println("Date de d�marrage : " + dateDemarrage);
				System.out.println("D�lai : " + ChronoUnit.SECONDS.between(dateDebut, dateDemarrage) + " Secondes");
				
				PRODUCTS_LIST.forEach(System.out::println);
			};
			
			displayerThreadService.schedule(command, 10, TimeUnit.SECONDS);
			
		} catch (Exception e) {
			System.out.println("L'ex�cution du thread ne s'est pas bien pass�e " + e.getMessage());
		} finally {
			if (displayerThreadService != null)
				displayerThreadService.shutdown();
		}

		// Traitement principal du thread main()
	}

}
