import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class ProductCounter2 {

	final static Map<String, Integer> PRODUCTS_MAP = new HashMap<String, Integer>();
	static {
		PRODUCTS_MAP.put("Miel", 1200);
		PRODUCTS_MAP.put("Chocolat", 522);
		PRODUCTS_MAP.put("Pomme", 347);
		PRODUCTS_MAP.put("Concombre", 701);
	};

	public static void main(String[] args) throws InterruptedException, ExecutionException {

		ExecutorService counterThreadService = Executors.newCachedThreadPool();
		try {
			//Execution du 1er thread
			Future<Integer> sommeProduits = counterThreadService.submit(() -> {
				Stream<Integer> streamValues = PRODUCTS_MAP.values().stream();
				Optional<Integer> optResult =  streamValues.reduce(Integer::sum);
				return optResult.isPresent() ? optResult.get() : null;
			});
			
			//Ajout d'un second thread pour execution
			Future<Integer> minimumProduits = counterThreadService.submit(() -> {
				Stream<Integer> streamValues = PRODUCTS_MAP.values().stream();
				Optional<Integer> optResult =  streamValues.min((Integer e1, Integer e2) -> e1.compareTo(e2) );
				return optResult.isPresent() ? optResult.get() : null;
			});
			
			//exploitation du r�sultat du thread counterThread.
			System.out.println("Nombre total de produits dans le magasin : " + sommeProduits.get());
			System.out.println("Nombre total de produits dans le magasin : " + minimumProduits.get());

			
		} catch (Exception e) {
			System.out.println("L'ex�cution du thread ne s'est pas bien pass�e " + e.getMessage());
		} finally {
			if (counterThreadService != null) counterThreadService.shutdown();
		}
				
		// Traitement principal du thread main()
	}

}