import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ProductDisplayer4 {

	final static List<String> PRODUCTS_LIST = Arrays.asList("Miel", "Chocolat", "Pomme", "Chocolat");
	static int INDEX = 0;

	public static void main(String[] args) {

		ScheduledExecutorService displayerThreadService = Executors.newSingleThreadScheduledExecutor();
		try { 
			System.out.println("Date de d�but : " + LocalDateTime.now());
			Runnable command = () -> {
					System.out.println("Article " + INDEX + " : " + PRODUCTS_LIST.get(INDEX) + " -> Affich� � la date : " + LocalDateTime.now());
					INDEX++;
					if(INDEX > 4) return;
			};
			
			displayerThreadService.scheduleAtFixedRate(command, 5, 10, TimeUnit.SECONDS);
			
			//on fait le processus main() attendre pendant 55 secondes
			//pour voir afficher le r�sultat des threads
			displayerThreadService.awaitTermination(55, TimeUnit.SECONDS);
			
		} catch (Exception e) {
			System.out.println("L'ex�cution du thread ne s'est pas bien pass�e " + e.getMessage());
		} finally {
			if (displayerThreadService != null) displayerThreadService.shutdown();
		}

		// Traitement principal du thread main()
	}

}
