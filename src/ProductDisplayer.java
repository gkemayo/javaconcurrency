import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ProductDisplayer {

	final static List<String> PRODUCTS_LIST = Arrays.asList("Miel", "Chocolat", "Pomme", "Chocolat");
	
	public static void main(String[] args) {
		
		ExecutorService displayerThreadService = Executors.newSingleThreadExecutor();
		try {
			displayerThreadService.execute(() -> {
				PRODUCTS_LIST.forEach(System.out::println);
			});
		}catch(Exception e) {
			System.out.println("L'ex�cution du thread ne s'est pas bien pass�e " + e.getMessage());
		}finally {
			if(displayerThreadService != null) displayerThreadService.shutdown();
		}

		//Traitement principal du thread main()
	}

}
