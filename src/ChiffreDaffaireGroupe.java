import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.BinaryOperator;
import java.util.function.LongSupplier;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class ChiffreDaffaireGroupe {

	static LongSupplier CHIFFRE_AFFAIRE_INDIV = () -> {
		//un chiffre d'affaire sera comprise entre 1000 et 2000 euros.
		return  1000 + Math.round(1000*Math.random());
	};
	//les 500 chiffres d'affaires des magasins du groupe
	final static LongStream LIST_CHIFFRES_AFF_INDIV = LongStream.generate(CHIFFRE_AFFAIRE_INDIV).limit(500);

	public static void main(String[] args) throws InterruptedException, ExecutionException {

		ExecutorService chiffreAffaireThreadsService = Executors.newFixedThreadPool(2);
		try {
			List<Long> listeChiffresDaffaires = LIST_CHIFFRES_AFF_INDIV.boxed().collect(Collectors.toList());
			Callable<Long> premierThread = () -> {
				return calculSommeIntermediaire(listeChiffresDaffaires.subList(0, 250), 1);
			};
			Callable<Long> secondThread = () -> {
				return calculSommeIntermediaire(listeChiffresDaffaires.subList(250, 500), 2);
			};
			List<Callable<Long>> listeThreadsAexecuter = Arrays.asList(premierThread, secondThread);
			
			List<Future<Long>> listChiffresDaffaireResultat = chiffreAffaireThreadsService.invokeAll(listeThreadsAexecuter);
			
			//exploitation du r�sultat des threads.
			Long chiffreDaffaireTotal = 0L;
			for (Future<Long> future : listChiffresDaffaireResultat) {
				chiffreDaffaireTotal += future.get();
			}
			System.out.println("Chiffre Affaire total du groupe : " + chiffreDaffaireTotal);
			
		} catch (Exception e) {
			System.out.println("L'ex�cution des threads ne s'est pas bien pass�e " + e.getMessage());
		} finally {
			if (chiffreAffaireThreadsService != null) chiffreAffaireThreadsService.shutdown();
		}
				
		// Traitement principal du thread main()
	}
	
	private static Long calculSommeIntermediaire(List<Long> list, int i) {
		BinaryOperator<Long> operateurSomme = (elt1, elt2) -> elt1 + elt2;
		Long chiffreAffaireIntermediaire2 = list.stream().reduce(0L, operateurSomme);
		System.out.println("Somme Chiffre d'affaire interm�diaire " + i + " : " + chiffreAffaireIntermediaire2);
		return chiffreAffaireIntermediaire2;
	}

}
