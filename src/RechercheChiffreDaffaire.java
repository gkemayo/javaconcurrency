import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.LongSupplier;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class RechercheChiffreDaffaire {
	static LongSupplier CHIFFRE_AFFAIRE_INDIV = () -> {
		//un chiffre d'affaire sera comprise entre 1000 et 2000 euros.
		return  1000 + Math.round(1000*Math.random());
	};
	//les 500 chiffres d'affaires des magasins du groupe
	final static LongStream LIST_CHIFFRES_AFF_INDIV = LongStream.generate(CHIFFRE_AFFAIRE_INDIV).distinct().limit(500);

	public static void main(String[] args) throws InterruptedException, ExecutionException {

		ExecutorService chiffreAffaireThreadsService = Executors.newFixedThreadPool(2);
		try {
			List<Long> listeChiffresDaffaires = LIST_CHIFFRES_AFF_INDIV.boxed().collect(Collectors.toList());
			Callable<String> premierThread = () -> {
				return direSiChiffreAffaireExiste(listeChiffresDaffaires.subList(0, 250), 1500L, 1);
			};
			Callable<String> secondThread = () -> {
				return direSiChiffreAffaireExiste(listeChiffresDaffaires.subList(250, 500), 1500L, 2);
			};
			List<Callable<String>> listeThreadsAexecuter = Arrays.asList(premierThread, secondThread);
			
			String resultat = chiffreAffaireThreadsService.invokeAny(listeThreadsAexecuter);
			
			//exploitation du r�sultat des threads.
			System.out.println(resultat);
			
		} catch (Exception e) {
			System.out.println("L'ex�cution des threads ne s'est pas bien pass�e " + e.getMessage());
		} finally {
			if (chiffreAffaireThreadsService != null) chiffreAffaireThreadsService.shutdown();
		}
		
		// Traitement principal du thread main()
	}
	
	private static String direSiChiffreAffaireExiste(List<Long> list, Long eltRecherche, int i) throws ExecutionException {
		if (list.contains(1500L)) {
			return "Le thread  " + i + " a trouv� le CA 1500 recherch�";
		}
		throw new ExecutionException("Aucun thread n a trouv� le CA 1500 recherch�", null);
	}

}
