import java.io.Console;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;

public class MainClass {

	public static void main(String[] args) throws IOException {
		
		Runnable myRunnable = () -> {
				for (int i = 0; i < 5; i++) {
					//corps du thread
					try {
						System.out.println("j'effectue un travail � l'instant : "+ System.currentTimeMillis());
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
		
		(new Thread(myRunnable)).start();
		
		System.out.println("free memory jvm : " + Runtime.getRuntime().freeMemory());
		System.out.println("total memory jvm : " + Runtime.getRuntime().totalMemory());
		System.out.println("max memory jvm : " + Runtime.getRuntime().maxMemory());
		System.out.println("nbre processors : " + Runtime.getRuntime().availableProcessors());
		//System.out.println("nbre processors : " + Runtime.getRuntime().exec("ipconfig"));
		Process p = Runtime.getRuntime().exec("ipconfig");
		InputStream in = p.getInputStream();
		System.out.println(in.read(new byte[1024]));
		
		ExecutorService se = null;

	}

}
